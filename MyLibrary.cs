﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW
{
    class MyLibrary
    {
        private Dictionary<string, Book> books = new Dictionary<string, Book>();
        public MyLibrary()
        { 
        }
        public bool AddBook(Book b)
        {
            if (books.ContainsValue(b))
            {
                return false;
            }
            else
            {
                books.Add(b.Title, b);
                return true;
            }
        }
        public bool RemoveBook(string x)
        {
            if (books.ContainsKey(x))
            {
                books.Remove(x);
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool HaveThisBook(string x)
        {
            if (books.ContainsKey(x))
                return true;
            else
                return false;
        }
        public Book GetBook(string x)
        {
            if (books.ContainsKey(x))
            {
                Book book_from_books = books[x];
                return book_from_books;
            }
            else
                return null;
        }
        public Book GetBookByAuthor(string x)
        {
            for (int i = 0; books.Count > i; i++)
            {
                if (books.TryGetValue(x, out Book b))
                {
                    return b;
                }
            }
            return null;
        }
        public void Clear()
        {
            books.Clear();
        }
        public List<string> GetAuthors()
        {
            List<string> authors = new list<string>();
            foreach (KeyValuePair<string, Book> x in books)
            {
                authors.Add(x.Value.Author);
            }
            return authors;
        }
        public List<Book> GetBooksSortedByAuthorName()
        {
            List<Book> b = new List<Book>();
            foreach (KeyValuePair<string, Book> x in books)
            {
                b.Add(x.Value);
            }
            b.Sort(new BookComparedByAuthor());
            return b;
        }
        public List<string> GetBooksTitleSorted()
        {
            List<string> b = new List<string>();
            foreach (KeyValuePair<string, Book> x in books)
            {
                b.Add(x.Value.Title);
            }
            b.Sort();
            return b;
        }
        public int Count
        {
            get
            {
                return books.Count;
            }
        }
        public override string ToString()
        {
           foreach(KeyValuePair<string,Book> item in books)
            {
                Console.WriteLine($"Book: {item.Value}");
            }
            return null;
        }
    }
}
